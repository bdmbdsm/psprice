FROM alpine:3.6
RUN apk update
RUN apk add python3
RUN mkdir -p /home/ps
WORKDIR /home/ps
COPY . .
RUN pip3 install -r requirements.txt
ENV PSPRICE_URL "https://retromagaz.com/playstation/playstation-4-2/konsoli-playstation-4-1/konsoli-playstation-4/playstation-4-slim-500gb"
CMD python3 psprice.py
