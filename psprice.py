import os
import requests
from bs4 import BeautifulSoup as bs

ENV_VAR_NAME = 'PSPRICE_URL'


def main():
    url = os.environ.get(ENV_VAR_NAME)
    if url is None:
        print('Set proper URL. Fix this stuff. Whatever.')
    resp = requests.get(url)
    html = resp.content
    soup = bs(html, 'html.parser')
    price_tag = soup.findAll('span', {'class': 'autocalc-product-price'})[0]
    price = price_tag.text
    print(price)


if __name__ == '__main__':
    main()
